import {useState, useEffect} from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

export default function Login() {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [isActive, setIsActive] = useState(false);

  function loginUser(e){
      e.preventDefault()
      setEmail("");
      setPassword("");

      alert('Login Successful!')
  }

  useEffect(() => {
      if(email !== "" && password !== ""){
          setIsActive(true);
      }
      else{
          setIsActive(false)
      }
  }, [email, password])

  return (
    <Form onSubmit={loginUser}>
      <Form.Group className="mb-3" controlId="email">
        <Form.Label>Email address</Form.Label>
        <Form.Control type="email" placeholder="Enter email" 
          value={email}
          onChange={e => setEmail(e.target.value)} 
          required/>
      </Form.Group>

      <Form.Group className="mb-3" controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Password" 
          value={password}
          onChange={e => setPassword(e.target.value)} 
          required/>
      </Form.Group>
        {isActive ? 
            <Button variant="primary" type="submit" id="submitBtn">
                Submit
            </Button>
            :
            <Button variant="danger" type="submit" id="submitBtn" disabled>
                Submit
            </Button>
        }
    </Form>
  );
}
